
#include "ZZSelection/ZZSelector.h"
#include "ZZSelection/Masses.h"

TLorentzVector ZZSelector::BuildZMuMu()
{
     TLorentzVector zCand(0.,0.,0.,0.);

     TLorentzVector mu0(0.,0.,0.,0.);
     TLorentzVector mu1(0.,0.,0.,0.);
     
     mu0.SetPtEtaPhiM(mu_staco_pT->at(0), mu_staco_eta->at(0), mu_staco_phi->at(0), Masses::muonMassPDG);
     mu1.SetPtEtaPhiM(mu_staco_pT->at(1), mu_staco_eta->at(1), mu_staco_phi->at(1), Masses::muonMassPDG);

     zCand = mu0 + mu1;
      

     return zCand;

}
