
#include "ZZSelection/ZZSelector.h"

void ZZSelector::SetInptBrchsZero()
{
    el_charge                         = 0;
    el_pT                             = 0;
    el_eta                            = 0;
    el_phi                            = 0;
    el_Reco_SF                        = 0;
    el_ID_SF                          = 0;
    el_isTriggerMatched               = 0;
    el_isEF_e24vhi_medium1Matched     = 0;
    el_isEF_e60_medium1Matched        = 0;
    mu_staco_charge                   = 0;
    mu_staco_pT                       = 0;
    mu_staco_eta                      = 0;
    mu_staco_phi                      = 0;
    mu_staco_Reco_SF                  = 0;
    mu_staco_isTriggerMatched         = 0;
    mu_staco_isEF_mu24i_tightMatched  = 0;
    mu_staco_isEF_mu36_tightMatched   = 0;
    jet_pT                            = 0;
    jet_eta                           = 0;
    jet_phi                           = 0;
    jet_m                             = 0;
    veto_elecs_pT                     = 0;
    veto_elecs_eta                    = 0;
    veto_elecs_phi                    = 0;
    veto_muons_pT                     = 0;
    veto_muons_eta                    = 0;
    veto_muons_phi                    = 0;
    ph_pT                             = 0;
    ph_cl_eta                         = 0;
    ph_cl_phi                         = 0;
    ph_etas2                          = 0;
    ph_isConverted                    = 0;
    ph_isTightPhoton                  = 0;
    photonCleanedJets_pT              = 0;
    photonCleanedJets_eta             = 0;
    photonCleanedJets_phi             = 0;
    photonCleanedJets_m               = 0;

} // end void SetInptBrchsZero
