////////////////////////////////////////////////////////////////////////////////
// file: HistGen.cxx
// author: Steven Kaneti Steven.KanetiSPAMNOTcern.ch
//
// Implementation file for HistGen class.
//
////////////////////////////////////////////////////////////////////////////////

#include <iostream>

#include "ZZSelection/BinDefs.h"
#include "ZZSelection/BinEdges.h"
#include "ZZSelection/HistGen.h"
#include "ZZSelection/SystemOfUnits.h"


// default constructor
HistGen::HistGen()
:
hgname("default"),
hgetmiss_eechan(NULL),
hgjetn_eechan(NULL),
hgaxialmet_eechan(NULL),
hgdphiphotz_eechan(NULL),
hgfracptdiff_eechan(NULL),
hgetmiss_mumuchan(NULL),
hgjetn_mumuchan(NULL),
hgaxialmet_mumuchan(NULL),
hgdphiphotz_mumuchan(NULL),
hgfracptdiff_mumuchan(NULL)
{

} // end default constructor

HistGen::HistGen(const std::string& name)
:
hgname(name),
hgetmiss_eechan(NULL),
hgjetn_eechan(NULL),
hgaxialmet_eechan(NULL),
hgdphiphotz_eechan(NULL),
hgfracptdiff_eechan(NULL),
hgetmiss_mumuchan(NULL),
hgjetn_mumuchan(NULL),
hgaxialmet_mumuchan(NULL),
hgdphiphotz_mumuchan(NULL),
hgfracptdiff_mumuchan(NULL)
{

    std::cout << "DEBUG:: (HistGen.cxx) in constructor" << std::endl;


    std::cout << "DEBUG:: (HistGen.cxx) name = " << name << std::endl;

    std::cout << "DEBUG:: (HistGen.cxx) in constructor booking histograms" << std::endl;
    hgjetn_eechan          = new TH1D((char*)"h_jetMultiplicity_eechan"    , (char*)"h_jetMultiplicity_eechan"     , bindefs::njetb       , binedges::jetnmin      , binedges::jetnmax);
    hgetmiss_eechan        = new TH1D((char*)"h_MET_RefFinalStd_eechan"    , (char*)"h_MET_RefFinalStd_eechan"     , bindefs::nMETb       , binedges::METmin       , binedges::METmax);
    hgaxialmet_eechan      = new TH1D((char*)"h_AxialMET_eechan"           , (char*)"h_AxialMET_eechan"            , bindefs::naxmetb     , binedges::axmetmin     , binedges::axmetmax);
    hgfracptdiff_eechan    = new TH1D((char*)"h_fracpTDiff_eechan"         , (char*)"h_fracpTDiff_eechan"          , bindefs::nfracdiffb  , binedges::nfracdiffmin , binedges::nfracdiffmax);
    hgdphiphotz_eechan     = new TH1D((char*)"h_deltaphiMETZ_eechan"       , (char*)"h_deltaphiMETZ_eechan"        , bindefs::ndphimetphb , binedges::dphimetphmin , binedges::dphimetphmax);

    hgjetn_mumuchan        = new TH1D((char*)"h_jetMultiplicity_mumuchan"  , (char*)"h_jetMultiplicity_mumuchan"   , bindefs::njetb       , binedges::jetnmin      , binedges::jetnmax);
    hgetmiss_mumuchan      = new TH1D((char*)"h_MET_RefFinalStd_mumuchan"  , (char*)"h_MET_RefFinalStd_mumuchan"   , bindefs::nMETb       , binedges::METmin       , binedges::METmax);
    hgaxialmet_mumuchan    = new TH1D((char*)"h_AxialMET_mumuchan"         , (char*)"h_AxialMET_mumuchan"          , bindefs::naxmetb     , binedges::axmetmin     , binedges::axmetmax);
    hgfracptdiff_mumuchan  = new TH1D((char*)"h_fracpTDiff_mumuchan"       , (char*)"h_fracpTDiff_mumuchan"        , bindefs::nfracdiffb  , binedges::nfracdiffmin , binedges::nfracdiffmax);
    hgdphiphotz_mumuchan   = new TH1D((char*)"h_deltaphiMETZ_mumuchan"     , (char*)"h_deltaphiMETZ_mumuchan"      , bindefs::ndphimetphb , binedges::dphimetphmin , binedges::dphimetphmax);
    std::cout << "DEBUG:: (HistGen.cxx) successfully booked histograms" << std::endl;

    hgjetn_eechan->Sumw2();
    hgetmiss_eechan->Sumw2();
    hgaxialmet_eechan->Sumw2();
    hgfracptdiff_eechan->Sumw2();
    hgdphiphotz_eechan->Sumw2();
    std::cout << "DEBUG:: (HistGen.cxx) called Sum Square of Weights for eechannel Histograms" << std::endl;

    hgjetn_mumuchan->Sumw2();
    hgetmiss_mumuchan->Sumw2();
    hgaxialmet_mumuchan->Sumw2();
    hgfracptdiff_mumuchan->Sumw2();
    hgdphiphotz_mumuchan->Sumw2();
    std::cout << "DEBUG:: (HistGen.cxx) called Sum Square of Weights for mumuchannel Histograms" << std::endl;

}

HistGen::~HistGen()
{

}

void HistGen::FillHists(const DataFields& df)
{

   // if running on data
   if (df.getisMCsim() == false) {
     hgjetn_eechan->Fill(df.getjetn());
     hgetmiss_eechan->Fill(df.getetmiss()/Units::GeV);
     hgaxialmet_eechan->Fill(df.getaxialetmiss()/Units::GeV);
     hgfracptdiff_eechan->Fill(df.getfracptdiff());
     hgdphiphotz_eechan->Fill(df.getdeltaphilepetmiss());

     hgjetn_mumuchan->Fill(df.getjetn());
     hgetmiss_mumuchan->Fill(df.getetmiss()/Units::GeV);
     hgaxialmet_mumuchan->Fill(df.getaxialetmiss()/Units::GeV);
     hgfracptdiff_mumuchan->Fill(df.getfracptdiff());
     hgdphiphotz_mumuchan->Fill(df.getdeltaphilepetmiss());
   } else {
     hgjetn_eechan->Fill(df.getjetn(), df.getMCweight());
     hgetmiss_eechan->Fill(df.getetmiss()/Units::GeV, df.getMCweight());
     hgaxialmet_eechan->Fill(df.getaxialetmiss()/Units::GeV, df.getMCweight());
     hgfracptdiff_eechan->Fill(df.getfracptdiff(), df.getMCweight());
     hgdphiphotz_eechan->Fill(df.getdeltaphilepetmiss(), df.getMCweight());
   
     hgjetn_mumuchan->Fill(df.getjetn(), df.getMCweight());
     hgetmiss_mumuchan->Fill(df.getetmiss()/Units::GeV, df.getMCweight());
     hgaxialmet_mumuchan->Fill(df.getaxialetmiss()/Units::GeV, df.getMCweight());
     hgfracptdiff_mumuchan->Fill(df.getfracptdiff(), df.getMCweight());
     hgdphiphotz_mumuchan->Fill(df.getdeltaphilepetmiss(), df.getMCweight());
   }

} // end void HistGen::FillHists(const DataFields& df)


void HistGen::FillHistsEEChan(const DataFields& df)
{

   if (df.getisMCsim() == false) {
     hgjetn_eechan->Fill(df.getjetn());
     hgetmiss_eechan->Fill(df.getetmiss()/Units::GeV);
     hgaxialmet_eechan->Fill(df.getaxialetmiss()/Units::GeV);
     hgfracptdiff_eechan->Fill(df.getfracptdiff());
     hgdphiphotz_eechan->Fill(df.getdeltaphilepetmiss());
   } else {
     hgjetn_eechan->Fill(df.getjetn(), df.getMCweight());
     hgetmiss_eechan->Fill(df.getetmiss()/Units::GeV, df.getMCweight());
     hgaxialmet_eechan->Fill(df.getaxialetmiss()/Units::GeV, df.getMCweight());
     hgfracptdiff_eechan->Fill(df.getfracptdiff(), df.getMCweight());
     hgdphiphotz_eechan->Fill(df.getdeltaphilepetmiss(), df.getMCweight());
   }
}

void HistGen::FillHistsMuMuChan(const DataFields& df)
{

   if (df.getisMCsim() == false) {
     hgjetn_mumuchan->Fill(df.getjetn());
     hgetmiss_mumuchan->Fill(df.getetmiss()/Units::GeV);
     hgaxialmet_mumuchan->Fill(df.getaxialetmiss()/Units::GeV);
     hgfracptdiff_mumuchan->Fill(df.getfracptdiff());
     hgdphiphotz_mumuchan->Fill(df.getdeltaphilepetmiss());
   } else {
     hgjetn_mumuchan->Fill(df.getjetn(), df.getMCweight());
     hgetmiss_mumuchan->Fill(df.getetmiss()/Units::GeV, df.getMCweight());
     hgaxialmet_mumuchan->Fill(df.getaxialetmiss()/Units::GeV, df.getMCweight());
     hgfracptdiff_mumuchan->Fill(df.getfracptdiff(), df.getMCweight());
     hgdphiphotz_mumuchan->Fill(df.getdeltaphilepetmiss(), df.getMCweight());
   }

}

void HistGen::WriteHists()
{
    //std::cout << "DEBUG:: (HistGen.cxx) In Function WriteHists()" << std::endl;

    //std::cout << "DEBUG:: (HistGen.cxx) In WriteHists(), Attempting to write out histograms" << std::endl;

    hgjetn_eechan->Write(); 
    hgetmiss_eechan->Write();
    hgaxialmet_eechan->Write();
    hgfracptdiff_eechan->Write();
    hgdphiphotz_eechan->Write();

    hgjetn_mumuchan->Write();
    hgetmiss_mumuchan->Write();
    hgaxialmet_mumuchan->Write();
    hgfracptdiff_mumuchan->Write();
    hgdphiphotz_mumuchan->Write();
   
} // end void HistGen::WriteHists() 
