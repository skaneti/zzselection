////////////////////////////////////////////////////////////////////////////////
//  file: DataFields.cxx
//  author: Steven Kaneti Steven.KanetiSPAMNOTcern.ch
//  
//  Implementation file for DataFields class.  The datafields class is passed
//  to the HistGen class and contains all the necessary fields to fill the
//  histograms of the HistGen class including photon pT, photon pT weights extracted from the
//  histograms which store the weights etc.  Data members are initialized in the 
//  constructor, although can be changed with set and get functions.
//
////////////////////////////////////////////////////////////////////////////////

#include <iostream>

#include "ZZSelection/DataFields.h"

DataFields::DataFields(double pTzboson, 
                       bool   simltn,
                       double mcwt,
                       double xm,
                       int    nj,
                       double axmet,
                       double dpml,
                       double fptd):
dfpTZ(pTzboson),
dfisMCsim(simltn),
dfMCweight(mcwt),
dfetmiss(xm),
dfjetn(nj),
dfaxialetmiss(axmet),
dfdeltaphilepetmiss(dpml),
dffracptdiff(fptd)
{

  //std::cout << "DEBUG (DataFields.cxx) In Constructor " << std::endl;
  //std::cout << "DEBUG (DataFields.cxx) Constructing DataFields object " << std::endl;

}

DataFields::~DataFields()
{

}

void DataFields::clearFields()
{
     dfpTZ               = 0;
     dfisMCsim           = 0;
     dfMCweight          = 0;
     dfetmiss            = 0;
     dfjetn              = 0;
     dfaxialetmiss       = 0;
     dfdeltaphilepetmiss = 0;
     dffracptdiff        = 0;
}

void DataFields::setpTZ(double zmomentum)
{
  dfpTZ = zmomentum;
}

void  DataFields::setisMCsim(bool simltn)
{
  dfisMCsim = simltn;
}

void  DataFields::setMCweight(double mcwt)
{
  dfMCweight = mcwt;
} 

void  DataFields::setetmiss(double xm)
{
  dfetmiss = xm;
}

void  DataFields::setjetn(int nj)
{
  dfjetn = nj;
}

void  DataFields::setaxialetmiss(double axmet)
{
  dfaxialetmiss = axmet;
}

void  DataFields::setdeltaphilepetmiss(double dpml)
{
  dfdeltaphilepetmiss = dpml;
}

void  DataFields::setfracptdiff(double fptd)
{
  dffracptdiff = fptd;
}

// get methods

double DataFields::getpTZ() const
{
  return dfpTZ;
}

bool DataFields::getisMCsim() const
{
  return dfisMCsim;
}

double DataFields::getMCweight() const
{
  return dfMCweight;
}

double DataFields::getetmiss() const
{
  return dfetmiss;
}

int    DataFields::getjetn() const
{
  return dfjetn;
}

double DataFields::getaxialetmiss() const
{
  return dfaxialetmiss;
}

double DataFields::getdeltaphilepetmiss() const
{
  return dfdeltaphilepetmiss;
}

double DataFields::getfracptdiff() const
{
  return dffracptdiff;
}
