
#include "ZZSelection/ZZSelector.h"


void ZZSelector::SetInptBrchAddrs()
{
    
    std::cout << "Setting branch addresses for input TTree" << std::endl;

    fChain->SetBranchAddress("RunNumber"                        , & RunNumber                        , & b_RunNumber);
    fChain->SetBranchAddress("EventNumber"                      , & EventNumber                      , & b_EventNumber);
    fChain->SetBranchAddress("mc_channel_number"                , & mc_channel_number                , & b_mc_channel_number);
    fChain->SetBranchAddress("dataStream"                       , & dataStream                       , & b_dataStream);
    fChain->SetBranchAddress("pileupWeight"                     , & pileupWeight                     , & b_pileupWeight);
    fChain->SetBranchAddress("averageIntPerXing"                , & averageIntPerXing                , & b_averageIntPerXing);
    fChain->SetBranchAddress("avgIntPerXingCorrected"           , & avgIntPerXingCorrected           , & b_avgIntPerXingCorrected);
    fChain->SetBranchAddress("mc_event_weight"                  , & mc_event_weight                  , & b_mc_event_weight);
    fChain->SetBranchAddress("zvtxweight"                       , & zvtxweight                       , & b_zvtxweight);
    fChain->SetBranchAddress("trigger_weight"                   , & trigger_weight                   , & b_trigger_weight);
    fChain->SetBranchAddress("el_n"                             , & el_n                             , & b_el_n);
    fChain->SetBranchAddress("el_charge"                        , & el_charge                        , & b_el_charge);
    fChain->SetBranchAddress("el_pT"                            , & el_pT                            , & b_el_pT);
    fChain->SetBranchAddress("el_eta"                           , & el_eta                           , & b_el_eta);
    fChain->SetBranchAddress("el_phi"                           , & el_phi                           , & b_el_phi);
    fChain->SetBranchAddress("el_Reco_SF"                       , & el_Reco_SF                       , & b_el_Reco_SF);
    fChain->SetBranchAddress("el_ID_SF"                         , & el_ID_SF                         , & b_el_ID_SF);
    fChain->SetBranchAddress("el_isTriggerMatched"              , & el_isTriggerMatched              , & b_el_isTriggerMatched);
    fChain->SetBranchAddress("el_isEF_e24vhi_medium1Matched"    , & el_isEF_e24vhi_medium1Matched    , & b_el_isEF_e24vhi_medium1Matched);
    fChain->SetBranchAddress("el_isEF_e60_medium1Matched"       , & el_isEF_e60_medium1Matched       , & b_el_isEF_e60_medium1Matched);
    fChain->SetBranchAddress("mu_staco_n"                       , & mu_staco_n                       , & b_mu_staco_n);
    fChain->SetBranchAddress("mu_staco_charge"                  , & mu_staco_charge                  , & b_mu_staco_charge);
    fChain->SetBranchAddress("mu_staco_pT"                      , & mu_staco_pT                      , & b_mu_staco_pT);
    fChain->SetBranchAddress("mu_staco_eta"                     , & mu_staco_eta                     , & b_mu_staco_eta);
    fChain->SetBranchAddress("mu_staco_phi"                     , & mu_staco_phi                     , & b_mu_staco_phi);
    fChain->SetBranchAddress("mu_staco_Reco_SF"                 , & mu_staco_Reco_SF                 , & b_mu_staco_Reco_SF);
    fChain->SetBranchAddress("mu_staco_isTriggerMatched"        , & mu_staco_isTriggerMatched        , & b_mu_staco_isTriggerMatched);
    fChain->SetBranchAddress("mu_staco_isEF_mu24i_tightMatched" , & mu_staco_isEF_mu24i_tightMatched , & b_mu_staco_isEF_mu24i_tightMatched);
    fChain->SetBranchAddress("mu_staco_isEF_mu36_tightMatched"  , & mu_staco_isEF_mu36_tightMatched  , & b_mu_staco_isEF_mu36_tightMatched);
    fChain->SetBranchAddress("jet_n"                            , & jet_n                            , & b_jet_n);
    fChain->SetBranchAddress("jet_pT"                           , & jet_pT                           , & b_jet_pT);
    fChain->SetBranchAddress("jet_eta"                          , & jet_eta                          , & b_jet_eta);
    fChain->SetBranchAddress("jet_phi"                          , & jet_phi                          , & b_jet_phi);
    fChain->SetBranchAddress("jet_m"                            , & jet_m                            , & b_jet_m);
    fChain->SetBranchAddress("MET_RefFinal_pT"                  , & MET_RefFinal_pT                  , & b_MET_RefFinal_pT);
    fChain->SetBranchAddress("MET_RefFinal_eta"                 , & MET_RefFinal_eta                 , & b_MET_RefFinal_eta);
    fChain->SetBranchAddress("MET_RefFinal_phi"                 , & MET_RefFinal_phi                 , & b_MET_RefFinal_phi);
    fChain->SetBranchAddress("MET_RefFinal_m"                   , & MET_RefFinal_m                   , & b_MET_RefFinal_m);
    fChain->SetBranchAddress("veto_elecs_n"                     , & veto_elecs_n                     , & b_veto_elecs_n);
    fChain->SetBranchAddress("veto_elecs_pT"                    , & veto_elecs_pT                    , & b_veto_elecs_pT);
    fChain->SetBranchAddress("veto_elecs_eta"                   , & veto_elecs_eta                   , & b_veto_elecs_eta);
    fChain->SetBranchAddress("veto_elecs_phi"                   , & veto_elecs_phi                   , & b_veto_elecs_phi);
    fChain->SetBranchAddress("veto_muons_n"                     , & veto_muons_n                     , & b_veto_muons_n);
    fChain->SetBranchAddress("veto_muons_pT"                    , & veto_muons_pT                    , & b_veto_muons_pT);
    fChain->SetBranchAddress("veto_muons_eta"                   , & veto_muons_eta                   , & b_veto_muons_eta);
    fChain->SetBranchAddress("veto_muons_phi"                   , & veto_muons_phi                   , & b_veto_muons_phi);
    fChain->SetBranchAddress("EF_e24vhi_medium1"                , & EF_e24vhi_medium1                , & b_EF_e24vhi_medium1);
    fChain->SetBranchAddress("EF_e60_medium1"                   , & EF_e60_medium1                   , & b_EF_e60_medium1);
    fChain->SetBranchAddress("EF_mu24i_tight"                   , & EF_mu24i_tight                   , & b_EF_mu24i_tight);
    fChain->SetBranchAddress("EF_mu36_tight"                    , & EF_mu36_tight                    , & b_EF_mu36_tight);
    fChain->SetBranchAddress("ph_n"                             , & ph_n                             , & b_ph_n);
    fChain->SetBranchAddress("ph_pT"                            , & ph_pT                            , & b_ph_pT);
    fChain->SetBranchAddress("ph_cl_eta"                        , & ph_cl_eta                        , & b_ph_cl_eta);
    fChain->SetBranchAddress("ph_cl_phi"                        , & ph_cl_phi                        , & b_ph_cl_phi);
    fChain->SetBranchAddress("ph_etas2"                         , & ph_etas2                         , & b_ph_etas2);
    fChain->SetBranchAddress("ph_isConverted"                   , & ph_isConverted                   , & b_ph_isConverted);
    fChain->SetBranchAddress("ph_isTightPhoton"                 , & ph_isTightPhoton                 , & b_ph_isTightPhoton);
    fChain->SetBranchAddress("EF_g20_loose"                     , & EF_g20_loose                     , & b_EF_g20_loose);
    fChain->SetBranchAddress("EF_g40_loose"                     , & EF_g40_loose                     , & b_EF_g40_loose);
    fChain->SetBranchAddress("EF_g60_loose"                     , & EF_g60_loose                     , & b_EF_g60_loose);
    fChain->SetBranchAddress("EF_g80_loose"                     , & EF_g80_loose                     , & b_EF_g80_loose);
    fChain->SetBranchAddress("EF_g100_loose"                    , & EF_g100_loose                    , & b_EF_g100_loose);
    fChain->SetBranchAddress("EF_g120_loose"                    , & EF_g120_loose                    , & b_EF_g120_loose);
    fChain->SetBranchAddress("photonCleanedJets_n"              , & photonCleanedJets_n              , & b_photonCleanedJets_n);
    fChain->SetBranchAddress("photonCleanedJets_pT"             , & photonCleanedJets_pT             , & b_photonCleanedJets_pT);
    fChain->SetBranchAddress("photonCleanedJets_eta"            , & photonCleanedJets_eta            , & b_photonCleanedJets_eta);
    fChain->SetBranchAddress("photonCleanedJets_phi"            , & photonCleanedJets_phi            , & b_photonCleanedJets_phi);
    fChain->SetBranchAddress("photonCleanedJets_m"              , & photonCleanedJets_m              , & b_photonCleanedJets_m);

}
