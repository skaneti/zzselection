////////////////////////////////////////////////////////////////////////////////
// file:    BuildZEE.cxx
// Author:  Steven Kaneti
// Created: 8 Mar 2013
//
////////////////////////////////////////////////////////////////////////////////

#include "ZZSelection/ZZSelector.h"
#include "ZZSelection/Masses.h"

TLorentzVector ZZSelector::BuildZEE() 
{
     TLorentzVector zCand(0.,0.,0.,0.);

     TLorentzVector e0(0.,0.,0.,0.);
     TLorentzVector e1(0.,0.,0.,0.);
     
     e0.SetPtEtaPhiM(el_pT->at(0), el_eta->at(0), el_phi->at(0), Masses::elecMassPDG);
     e1.SetPtEtaPhiM(el_pT->at(1), el_eta->at(1), el_phi->at(1), Masses::elecMassPDG);

     zCand = e0 + e1;
      

     return zCand;

}
