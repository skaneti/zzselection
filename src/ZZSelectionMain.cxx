////////////////////////////////////////////////////////////////////////////////
// file:    ZZSelectionMain.cxx
// Author:  Steven Kaneti
// Created: 7 Mar 2013
//
////////////////////////////////////////////////////////////////////////////////

#include <algorithm>
#include <assert.h>
#include <cstdlib>
#include <cstdio>
#include <fstream>
#include <iostream>
#include <map>
#include <string>
#include <unistd.h>
#include <vector>
#include <getopt.h>

#include "TROOT.h"
#include "TChain.h"
#include "TFile.h"
#include "TTree.h"
#include "TMath.h"

#include "ZZSelection/ZZSelector.h"
#include "ZZSelection/HistGen.h"

int main(int argc, char* argv[])
{
    const char* optstring = "dt:f:T:o:D:m";
    

    bool isMC                 = false;
    bool debug                = true;
    bool saveLumiInfo         = true;
    char* inputTTree          = (char*)"zztree";
    char* inputFileName       = (char*)"";
    char* inputTChain         = (char*)"";
    char* outputFileName      = (char*)"zselxnspectra.root";
    int   nptb                = 120;
    int   nextrabins          = 0;
    int   binspacing          = 5;   
   
    /* by default set the output directory to the current working directory 
     * this will be overwritten if the command line option --outputFileDirectory is set
     */
    char *outputFileDirectory = NULL;
    size_t size;
    outputFileDirectory = getcwd(outputFileDirectory, size);

    static struct option long_options[] = 
    {
      /* name    has_arg       flag      val */
      {"debug"              , no_argument      ,      NULL,     'd'},
      {"inputTChain"        , required_argument,      NULL,     't'},
      {"inputFile"          , required_argument,      NULL,     'f'},
      {"inputTTree"         , required_argument,      NULL,     'T'},
      {"outputFileName"     , required_argument,      NULL,     'o'},
      {"outputFileDirectory", required_argument,      NULL,     'D'},
      {"isMC"               , no_argument      ,      NULL,     'm'},
      { NULL                , no_argument      ,      NULL,      0 },
    };
    
    int longindex;
    int option;
    int begin = 0;
    int end   = 999;

    while ((option = getopt_long(argc, argv,
            optstring, long_options, &longindex)) != -1) {
        switch (option) {
            case 'd' :
              debug = true;
              break;
            case 't' :
              inputTChain = optarg;
            case 'f' :
              inputFileName = optarg;
              break;
            case 'T' :
              inputTTree = optarg;
              break;
            case 'o':
              outputFileName = optarg;
              break;
            case 'D':
              outputFileDirectory = optarg;
              break;
            case 'm' :
              isMC = true;
            case 0 :
              break;
            case '?' : 
              printf("Please supply a valid option to the program.  Exiting\n");
              exit(1); 
              break;
        }
    }

    gROOT->ProcessLine("#include <vector>");


    char* outFileLoc = (char*)malloc((strlen(outputFileDirectory) + strlen(outputFileName) + 4)*sizeof(char));
    strcpy(outFileLoc, outputFileDirectory);
    strcat(outFileLoc, "/");
    strcat(outFileLoc, outputFileName);
    
    TFile* ofile = new TFile(outFileLoc, "Recreate");
    printf("Output File Location is set to %s\n", outFileLoc);

    std::cout << "Attempting to creating new TChain with tree name " << inputTTree << std::endl;
    TChain* myChain = new TChain(inputTTree); 

    std::cout << "Successfully created new TChain with tree name " << inputTTree << std::endl;
    

    // Require at least 1 argument: input text file with list of root files
    // Each line can be a comma separated list of one or more files
    //if (strcmp(inputTChain, "") != 0 && strcmp(inputFileName, "") == 0) { 
        std::cout << "Adding files " << inputTChain << " to TChain" << std::endl;
        myChain->Add(inputTChain);
    //}

    if (strcmp(inputFileName, "") == 0 && strcmp(inputTChain, "") == 0) {
      std::cout << "You must supply an input file name with the list of files to be executed"
                << std::endl;
      std::cout << "Exiting Skimmer" << std::endl;
      exit(1);
    } else if (strcmp(inputFileName, "") != 0 && strcmp(inputTChain, "") == 0) {
         
      //FILE* istream;
      //if ((istream = fopen(inputFileName, "r" ) ) == NULL) {
      //   printf ( "file non-existant!\n" );
      //   exit(1);
      //} else {
      //   printf ("found file");
      //   fclose istream;
      //}

      std::string line;

      std::fstream inFile;
          
      inFile.open(inputFileName);
          
      if (inFile.is_open() == true) {
        std::cout << "found file " << inputFileName << std::endl;
      } else {
        std::cout << "unable to find file " << inputFileName << std::endl;
        exit(1); 
      }
 
      // Read lines in input line
      while (inFile >> line) {

        // Split each line by ","
        // Thanks to Nick Barlow!!
        for (size_t i = 0,n; i <= line.length(); i = n + 1) {
          n = line.find_first_of(',',i);
          if (n == std::string::npos) {
            n = line.length();
          }

          std::string tmp = line.substr(i,n-i);

          // Add file to chain
          std::cout << "Adding file to chain: " << tmp << std::endl;
          myChain->Add(tmp.c_str());
        }
      } // end while

      inFile.close();
         
       
    } // end else if (!inputFileName.empty()) 

   // Turn off all branches initially, then use function to turn on
   // branches that we want
   //myChain->SetBranchStatus("*", 0);
   if (debug) std::cout << "ZZSelectionMain.cxx DEBUG:: Turning off all branches" << std::endl;

   Long64_t nEntries = myChain->GetEntries();
   if (debug) std::cout << "DEBUG:: (ZZSelectionMain.cxx) nEntries = " << nEntries << std::endl;

    
   //if (nEntries == 0) {
   //  printf("TTree %s is empty. Exiting now.", inputTTree);
   //  exit(1);
   //} else if (myChain->LoadTree(0) < 0) {
   //  printf("%s->LoadTree() failed. No Valid TTree found. Exiting now\n", inputTTree);
   //  exit(1);
   //}
   // choose the number of pTbins for the kinematic plots
   // make a C-style array with nptbins + 1 entries with the
   // lower bound of the lowest bin and the upper edge of the highest bin


   HistGen zzhistos("zzhistos");

   std::vector<HistGen>* hgv = new std::vector<HistGen>(0);
   hgv->clear();


   if (debug) std::cout << "DEBUG:: (ZZSelectionMain.cxx) Pushing back HistGenObjects to vector" << std::endl;
   hgv->push_back(zzhistos);

   if (debug) std::cout << "DEBUG:: (ZZSelectionMain.cxx) Successfully pushed back HistGenObjects to vector" << std::endl;
  

   if (debug) std::cout << "DEBUG:: (ZZSelectionMain.cxx) Instantiating ZZSelector object" << std::endl;
   ZZSelector zzslctr(isMC, 
                      hgv,     
                      false);

   zzslctr.Init(myChain);

   int processCounter = 0;
   //begin loop over events
   for (Long64_t jentry = 0; jentry < nEntries; jentry++) { 
     myChain->GetEntry(jentry);
     zzslctr.Process(jentry); 
     //Long64_t local_entry = myChain->LoadTree(jentry);
     //if (local_entry==0) gj.NewTree();
     processCounter++;
   }

   std::cout << "DEBUG:: (ZZSelectionMain.cxx) calling cd() on output file" << std::endl; 
   ofile->cd();

   std::cout << "DEBUG:: (ZZSelectionMain.cxx) Calling Terminate on ZZSelector" << std::endl;
   zzslctr.Terminate();

   std::cout << "DEBUG:: (ZZSelectionMain.cxx) Process() counter = " << processCounter << std::endl;

   ofile->Write(); // write out all objects in memory to the output file
   std::cout << "DEBUG:: (ZZSelectionMain.cxx) calling Write() on output file" << std::endl; 

   ofile->Close();
   std::cout << "DEBUG:: (ZZSelectionMain.cxx) closing output file" << std::endl; 
    
   // free dynamically allocated memory

   std::cout << "DEBUG:: ZZSelectionMain.cxx deleting pointers for TFile" << std::endl;
   if (ofile)   delete ofile;
   std::cout << "DEBUG:: ZZSelectionMain.cxx deleting pointer for TChain " << std::endl;
   if (myChain) delete myChain;

   if (debug) std::cout << "DEBUG:: ZZSelectionMain.cxx Deleting TChain fChain " << std::endl;

   if (debug) std::cout << "DEBUG:: ZZSelectionMain.cxx Leaving Ntuple Maker" << std::endl;

   //indicate successful program termination
   return 0;

} // end main
////////////////////////////////////////////////////////////////////////////////
