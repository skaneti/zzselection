
#include "ZZSelection/ZZSelector.h"
#include "ZZSelection/SystemOfUnits.h"
#include "ZZSelection/DataFields.h"

// constructor
ZZSelector::ZZSelector(bool mc,
                       std::vector<HistGen>* histgenvec,
                       bool gjdbg)
:
isMC(mc),
hgvec(histgenvec),
debug(gjdbg),
nInputFile(0)
{
    
   if(debug) std::cout << "GammaJeSelector.cxx Debug:: Calling Constructor " << std::endl;
   SetTChain(0);

} // end constructor

ZZSelector::~ZZSelector()
{
    std::cout << "DEBUG:: ZZSelector.cxx Calling Destrcutor" << std::endl;
 
} // end destructor

void ZZSelector::Begin(TChain *fChain)
{

   TString option = GetOption();

} // end void ZZSelector::Begin()

void ZZSelector::SlaveBegin(TChain *fChain)
{

   TString option = GetOption();

} // end ZZSelector::SlaveBegin()

void ZZSelector::Init(TChain* ch)
{
    if (debug) std::cout << "DEBUG:: (ZZSelector.cxx) Start of Function Init" << std::endl;
            
    SetTChain(ch);
    //SetInptBrchStatus(); 
  
    if (isMC)
      std::cout << "Treating file as MC" << std::endl; 
    else
      std::cout << "Treating file as DATA" << std::endl;
    // Properties go here


    //gROOT->ProcessLine("#include <vector>");

    NewTree(); 

    if (debug) {
      std::cout << "DEBUG:: (ZZSelector.cxx) Successfully Initialized" << std::endl;
      std::cout << "DEBUG:: (ZZSelector.cxx) End of function Init"     << std::endl;
    }
} // end void ZZSelector::Init()

Bool_t ZZSelector::Notify()
{

   std::cout << "DEBUG::(ZZSelector.cxx) In Function Notify" << std::endl;
   std::cout << "DEBUG::(ZZSelector.cxx) Adding New File" << std::endl;

   return kTRUE;
}

Bool_t ZZSelector::Process(Long64_t entry)
{

   if (entry < 0) {
       if (debug) std::cout << "DEBUG:: (ZZSelector.cxx) Negative entry (Negative Event)" << std::endl;
           return kFALSE;
   }

   if (entry % 1000000 == 0) { 
       //std::cout << "Getting Run number for entry " << entry << std::endl;
       //std::cout << "RunNumber = " << RunNumber << std::endl;
       std::cout << " nInputFile  = " << std::setw(4) << nInputFile;
       std::cout << " entry = "       << std::setw(13) << entry;
       std::cout << " RunNumber = "   << std::setw(13) << RunNumber;
       std::cout << " EventNumber = " << std::setw(13) << EventNumber << std::endl;
       //std::cout << " Lumi Block Number (lbn) = " <<  std::setw(10) << lbn << std::endl; 
   }
   
   //fChain->GetTree()->GetEntry(entry); 
   //GetEntryQuick(entry);
   //std::cout << "DEBUG:: ZZSelector.cxx Getting Current Entry" << std::endl;

   //std::cout << "DEBUG:: ZZSelector.cxx Processing Current Entry" << std::endl;

   //if (debug && printBasicEvtInfo) {
   //  if (entry % 10 == 0) {
   //    std::cout << " nInputFile  = " << std::setw(6) << nInputFile;
   //    std::cout << " entry = "       << std::setw(8) << entry;
   //    std::cout << " RunNumber = "   << std::setw(9) << RunNumber;
   //    std::cout << " EventNumber = " << std::setw(10) << EventNumber << std::endl;
   //    //std::cout << " Lumi Block Number (lbn) = " <<  std::setw(10) << lbn << std::endl; 
   //  }
   //} 

   TLorentzVector zeechan( 0., 0., 0., 0.);
   
   if (el_n == 2 && mu_staco_n == 0) {
       
     if (!(el_charge->at(0)*el_charge->at(1) < 0)) return kTRUE;
     
     float leadingElecpT     = -9876.;
     float leadingEleceta    = -9876.;
     float leadingElecphi    = -9876.;
     float subLeadingElecpT  = -9876.;
     float subLeadingEleceta = -9876.;
     float subLeadingElecphi = -9876.; 
     if (el_pT->at(0) > el_pT->at(1)) {
       leadingElecpT       = el_pT->at(0);
       leadingEleceta      = el_eta->at(0);
       leadingElecphi      = el_phi->at(0);
       subLeadingElecpT    = el_pT->at(1);
       subLeadingEleceta   = el_eta->at(1);
       subLeadingElecphi   = el_phi->at(1);
     } else {
       leadingElecpT       = el_pT->at(1);  
       leadingEleceta      = el_eta->at(1);
       leadingElecphi      = el_phi->at(1);
       subLeadingElecpT    = el_pT->at(0);
       subLeadingEleceta   = el_eta->at(0);
       subLeadingElecphi   = el_phi->at(0);
     }       
  
     TLorentzVector ZeeChan = BuildZEE();

     if (!(ZeeChan.Pt() > 25*Units::GeV)) return kTRUE;
     
     TLorentzVector metRefFinal(0.,0.,0.,0.);
     metRefFinal.SetPtEtaPhiM(MET_RefFinal_pT, MET_RefFinal_eta, MET_RefFinal_phi, 0);
     
     double deltaphiMETZ = ZeeChan.DeltaPhi(metRefFinal);

     double axialMET = (-1)*metRefFinal.Et()*(std::cos(deltaphiMETZ));
 
     double fracpTDiff = ((std::fabs(metRefFinal.Et() - ZeeChan.Pt()))/(ZeeChan.Pt())); 
     
     double mcweight = 1.0;

     if (isMC) {
       mcweight =  el_Reco_SF->at(0)*el_Reco_SF->at(1)*el_ID_SF->at(0)*el_ID_SF->at(1)*
                   mc_event_weight*pileupWeight*zvtxweight*trigger_weight;
     }

     DataFields df(ZeeChan.Pt(), isMC, mcweight, MET_RefFinal_pT, jet_n, axialMET, deltaphiMETZ, fracpTDiff);

     if (!(leadingElecpT > 25*Units::GeV)) return kTRUE; 

     if (!(el_isTriggerMatched->at(0) || el_isTriggerMatched->at(1))) return kTRUE; 

     if (!(ZeeChan.Mag() > 15*Units::GeV)) return kTRUE;

     // apply mass window cut     
     // fill the Z mass histogram  and Z pT histogram in the ee channel after the mass cut
     if (!((ZeeChan.Mag() > 76*Units::GeV) &&  (ZeeChan.Mag() < 106*Units::GeV))) return kTRUE;

     // make the Z Distributions after the cuts that were used to derive the photon pT weights
     for (std::vector<HistGen>::iterator itr = hgvec->begin(); itr != hgvec->end(); itr++)
       (*itr).FillHistsEEChan(df); 
     
     //if (!(axialMET > 90.e3)) return kTRUE;
  
  } else if (mu_staco_n == 2 && el_n == 0) {
     
    if (!(mu_staco_charge->at(0)*mu_staco_charge->at(1) < 0)) return kTRUE;
    
    double leadingMuonpT     = -98760.;
    double leadingMuoneta    = -98760.;
    double leadingMuonphi    = -98760.;
    double subLeadingMuonpT  = -98760.;
    double subLeadingMuoneta = -98760.;
    double subLeadingMuonphi = -98760.;
    if (mu_staco_pT->at(0) > mu_staco_pT->at(1)) {
      leadingMuonpT     = mu_staco_pT->at(0);
      leadingMuoneta    = mu_staco_eta->at(0);
      leadingMuonphi    = mu_staco_phi->at(0);
      subLeadingMuonpT  = mu_staco_pT->at(1);
      subLeadingMuoneta = mu_staco_eta->at(1);
      subLeadingMuonphi = mu_staco_phi->at(1);
    } else {
      leadingMuonpT     = mu_staco_pT->at(1);
      leadingMuoneta    = mu_staco_eta->at(1);
      leadingMuonphi    = mu_staco_phi->at(1);
      subLeadingMuonpT  = mu_staco_pT->at(0);
      subLeadingMuoneta = mu_staco_eta->at(0);
      subLeadingMuonphi = mu_staco_phi->at(0);
    }
    
    TLorentzVector ZmumuChan = BuildZMuMu();
    
    if (!(ZmumuChan.Pt() > 25*Units::GeV)) return kTRUE;
    
    TLorentzVector metRefFinal(0.,0.,0.,0.);
    metRefFinal.SetPtEtaPhiM(MET_RefFinal_pT, MET_RefFinal_eta, MET_RefFinal_phi, 0);

    double deltaphiMETZ = ZmumuChan.DeltaPhi(metRefFinal);

    double axialMET = (-1)*metRefFinal.Et()*(std::cos(deltaphiMETZ));

    double fracpTDiff = ((std::fabs(metRefFinal.Et() - ZmumuChan.Pt()))/(ZmumuChan.Pt())); 
    
    double mcweight = 1.0;
    
    if (isMC) { 
       mcweight = mu_staco_Reco_SF->at(0)*mu_staco_Reco_SF->at(1)*
                  mc_event_weight*pileupWeight*zvtxweight*trigger_weight;
    }

    DataFields df(ZmumuChan.Pt(), isMC, mcweight, MET_RefFinal_pT, jet_n, axialMET, deltaphiMETZ, fracpTDiff);

    if (!(mu_staco_isTriggerMatched->at(0) || mu_staco_isTriggerMatched->at(1))) return kTRUE;

    if (!(ZmumuChan.Mag() > 15*Units::GeV)) return kTRUE;

    // apply mass window cut     
    if (!((ZmumuChan.Mag() > 76*Units::GeV) &&  (ZmumuChan.Mag() < 106*Units::GeV))) return kTRUE;	    

    for (std::vector<HistGen>::iterator itr = hgvec->begin(); itr != hgvec->end(); itr++)
      (*itr).FillHistsMuMuChan(df); 

    //if (!(axialMET > 90.e3)) return kTRUE;

  }
 
  return kTRUE;
}  // end ZZSelector::Process() 


void ZZSelector::SlaveTerminate()
{
   // The SlaveTerminate() function is called after all entries or objects
   // have been processed. When running with PROOF SlaveTerminate() is called
   // on each slave server.
   std::cout << "DEBUG:: (ZZSelector.cxx) In Function SlaveTerminate" << std::endl;

} // end void ZZSelector::SlaveTerminate()

void ZZSelector::Terminate()
{
   // The Terminate() function is the last function to be called during
   // a query. It always runs on the client, it can be used to present
   // the results graphically or save the results to file.
   std::cout << "DEBUG:: (ZZSelector.cxx) In Function Terminate" << std::endl;

   //std::cout << "DEBUG:: (ZZSelector.cxx) Writing out TFile" << std::endl;
} // end void ZZSelector::Terminate()


void ZZSelector::NewTree()
{
    std::cout << "Found New Tree" << std::endl;

    //Set Object Pointers
    SetInptBrchsZero();
    
    // Object Branch Addresses for Branches of input TTree
    SetInptBrchAddrs(); 
    
    nInputFile++;
}  // end void ZZSelector::NewTree()

void ZZSelector::SetTChain(TChain* ch)
{
    fChain = ch;
}

TChain* ZZSelector::GetTChain()
{
    return fChain;
}
