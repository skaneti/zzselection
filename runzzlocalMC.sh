#!/bin/bash

scriptname=runzzlocal
echo "sourcing $scriptname on `date -u`"
echo "current working directory is `pwd`" 
echo "looking for sub-directories in /r02/atlas/skaneti"


zzBaseDir="/r02/atlas/skaneti/ZZoutputGrid"
outputDir="/r02/atlas/skaneti/ZZoutputGrid/ZZSelection/MC"

#tag='2013Mar271519'
#tag='2013Apr170358'
tag="2013Apr041540"

executable=zzselector


for j in `ls -d $zzBaseDir/*MCID*$tag*`
do
  #inputDir=`echo $j | awk 'BEGIN { FS="/" } { print $6 }'`
  #MCID=`echo $inputDir | awk 'BEGIN {FS="."} {print $4}'`
  MCID=`echo $j | awk 'BEGIN {FS="."} {print $4}'`
  timestamp=`echo $j | awk 'BEGIN {FS="."} {print $5}'`
  downloadtag=`echo $j | awk 'BEGIN {FS="."} {print $6}'` 
  echo "timestamp=$timestamp"
  #downloadID=`echo $inputDir | awk 'BEGIN {FS="."} {print $6}'`
  outROOTFile="zzselexn."$MCID"."$timestamp"."$downloadtag".root"
  #echo "./$executable --isMC --inputTChain=$j/*.root --outputFileDirectory=$outputDir --outputFileName=$outROOTFile"
  #./$executable --inputTChain=$j/*.root --outputFileDirectory=$outputDir --outputFileName=$outROOTFile
  echo "Running ./$executable --isMC --inputTChain=$j/*.root --outputFileDirectory=$outputDir --outputFileName=$outROOTFile"
  ./$executable --isMC --inputTChain=$j/*.root --outputFileDirectory=$outputDir --outputFileName=$outROOTFile
done
