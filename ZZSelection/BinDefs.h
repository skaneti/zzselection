////////////////////////////////////////////////////////////////////////////////
// file: HistGen.cxx
// author: Steven Kaneti Steven.KanetiSPAMNOTcern.ch
//
// Number of bins for each type of histogram genereated.  The various types of histograms
// are mentioned below.  The numbers of bins are put in their own namespace not to be
// confused with the bin edges.  To change the number of bins for a given histogram, 
// simply edit the appropriate variable for the cooresponding histogram.
//
////////////////////////////////////////////////////////////////////////////////

#ifndef BINDEFS_H
#define BINDEFS_H

// number of bins for each type of histogram
// jet multiplicity, 
// missing ET
// axial missing ET
// fractional pT difference
// dphi(MET, photon)

namespace bindefs
{

   static int njetb       =  25;
   static int nMETb       =  70;
   static int naxmetb     = 110;
   static int nfracdiffb  =  13;
   static int ndphimetphb =  69;

}
#endif // BINDEFS_H
