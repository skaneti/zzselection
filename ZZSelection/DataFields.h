////////////////////////////////////////////////////////////////////////////////
// file: DataFields.h
// author: Steven Kaneti Steven.KanetiSPAMNOTcern.ch
// 
// This class stores the various data fields that are to be put into histograms
// (aside from photon pT).  This object will be passed to a HistGen object
// which will then fill the histogram  for the appropriate hoton pT bin
//
////////////////////////////////////////////////////////////////////////////////

#ifndef DATAFILEDS_H
#define DATAFILEDS_H

class DataFields 
{
   public:
     DataFields(double pTzboson, 
                bool   simltn,
                double mcwt,
                double xm,
                int    nj,
                double axmet,
                double dpml,
                double fptd);
     ~DataFields();
     
     void   clearFields(); 

     void   setpTZ(double zmomentum);                  //{dfphotonpT = photpt;}
     void   setisMCsim(bool simltn);                   //{dfisMCsim = simltn;}
     void   setMCweight(double mcwt);                  //{dfMCweight = mcwt;} 
     void   setetmiss(double xm);                      //{dfetmiss = xm;}
     void   setjetn(int nj);                           //{dfjetn = nj;}
     void   setaxialetmiss(double axmet);              //{dfaxialetmiss = axmet;}
     void   setdeltaphilepetmiss(double dpml);         //{dfdeltaphilepetmiss = dpml;}
     void   setfracptdiff(double fptd);                //{dffracptdiff = fptd;}

     double getpTZ()               const;      //{return dfphotonpT;}
     bool   getisMCsim()           const;      //{return dfisMCsim;}
     double getMCweight()          const;      //{return dfMCweight;} 
     double getetmiss()            const;      //{return dfetmiss;}
     int    getjetn()              const;      //{return dfjetn;}
     double getaxialetmiss()       const;      //{return dfaxialetmiss;}
     double getdeltaphilepetmiss() const;      //{return dfdeltaphilepetmiss;}
     double getfracptdiff()        const;      //{return dffracptdiff;}

   private:
     double dfpTZ;
     bool   dfisMCsim;
     double dfMCweight;
     double dfetmiss;
     int    dfjetn;
     double dfaxialetmiss;
     double dfdeltaphilepetmiss;
     double dffracptdiff;
     

};

#endif // DATAFILEDS_H
