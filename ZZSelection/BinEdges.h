
#ifndef BINEDGES_H
#define BINEDGES_H

#include "TMath.h"


namespace binedges
{

    static int    jetnmin             = 0;
    static int    jetnmax             = 24;

    static int    METmin              = 0;
    static int    METmax              = 350;

    static int    axmetmin            = -200;
    static int    axmetmax            = 350;

    static int    dphimetphmin        = -TMath::Pi()*1.10;
    static int    dphimetphmax        = TMath::Pi()*1.10;

    static float  nfracdiffmin        = 0;
    static float  nfracdiffmax        = 1.2; 


}

#endif // BINEDGES_H
