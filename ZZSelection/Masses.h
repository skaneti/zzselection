////////////////////////////////////////////////////////////////////////////////
//   file: Masses.h
//   author: Steven Kaneti
//   
//   Description: define particle masses from the PDG here.  Needs SystemOfUnits,
//   which should be in the directory with the other required header files.
//
//
////////////////////////////////////////////////////////////////////////////////

#ifndef MASSES_H
#define MASSES_H 

#include "ZZSelection/SystemOfUnits.h"


namespace Masses {
   static const double elecMassPDG = 0.510998910*Units::MeV; // units of MeV
   static const double muonMassPDG = 105.658367*Units::MeV;  // units of MeV

   // express in GeV, but convert to units of MeV; 
   // will evaluate to 91.1876e+03   
   static const double zMassPDG    = 91.1876*Units::GeV;     

}

#endif /* MASSES_H */

