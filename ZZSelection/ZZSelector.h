#ifndef ZZSelector_H
#define ZZSelector_H

// std includes
#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <cmath>

#include "TROOT.h"
#include "TChain.h"
#include "TFile.h"
#include "TSelector.h"
#include "TBranch.h"
#include "TMath.h"
#include "TVector3.h"
#include "TObjString.h"
#include "TString.h"
#include "TLorentzVector.h"
#include "TH1D.h"

#include "ZZSelection/HistGen.h"


class ZZSelector : public TSelector {
public:
     ZZSelector(bool mc,
                std::vector<HistGen>* histgenvec,
                bool gjdbg = true);

     virtual         ~ZZSelector();
     virtual void    Begin(TChain* fChain);
     virtual void    SlaveBegin(TChain* fChain);
     virtual void    Init(TChain* fChain);
     virtual Bool_t  Notify();
     virtual Bool_t  Process(Long64_t entry);
     virtual Int_t   GetEntry(Long64_t entry, Int_t getall = 0) { return fChain ? fChain->GetTree()->GetEntry(entry, getall) : 0; }
     virtual void    SetOption(const char *option) { fOption = option; }
     virtual void    SetObject(TObject *obj)       { fObject = obj; }
     virtual void    SetInputList(TList *input)    { fInput = input; }
     virtual TList   *GetOutputList() const        { return fOutput; }
     virtual void    SlaveTerminate();
     virtual void    Terminate();
     virtual Int_t   Version() const { return 2; } 
     
     virtual void    SetTChain(TChain* ch);
     TChain*         GetTChain();
     void            NewTree();

     void            SetInptBrchsZero();
     void            SetInptBrchAddrs();
     void            SetInptBrchStatus();

     TLorentzVector  BuildZEE();

     TLorentzVector  BuildZMuMu();
                                

private: 
     bool debug;

     //!pointer to the analyzed TTree or TChain
     TChain* fChain;

     int nInputFile;

     bool isMC;

     std::vector<HistGen>* hgvec;
 

     // declaration of leaf types
     // Declaration of leaf types
     UInt_t               RunNumber;
     UInt_t               EventNumber;
     UInt_t               mc_channel_number;
     Int_t                dataStream;
     Double_t             pileupWeight;
     Double_t             averageIntPerXing;
     Double_t             avgIntPerXingCorrected;
     Double_t             mc_event_weight;
     Double_t             zvtxweight;
     Double_t             trigger_weight;
     Int_t                el_n;
     std::vector<float>   *el_charge;
     std::vector<double>  *el_pT;
     std::vector<double>  *el_eta;
     std::vector<double>  *el_phi;
     std::vector<double>  *el_Reco_SF;
     std::vector<double>  *el_ID_SF;
     std::vector<bool>    *el_isTriggerMatched;
     std::vector<bool>    *el_isEF_e24vhi_medium1Matched;
     std::vector<bool>    *el_isEF_e60_medium1Matched;
     Int_t                mu_staco_n;
     std::vector<float>   *mu_staco_charge;
     std::vector<double>  *mu_staco_pT;
     std::vector<double>  *mu_staco_eta;
     std::vector<double>  *mu_staco_phi;
     std::vector<double>  *mu_staco_Reco_SF;
     std::vector<bool>    *mu_staco_isTriggerMatched;
     std::vector<bool>    *mu_staco_isEF_mu24i_tightMatched;
     std::vector<bool>    *mu_staco_isEF_mu36_tightMatched;
     Int_t                jet_n;
     std::vector<double>  *jet_pT;
     std::vector<double>  *jet_eta;
     std::vector<double>  *jet_phi;
     std::vector<double>  *jet_m;
     Double_t             MET_RefFinal_pT;
     Double_t             MET_RefFinal_eta;
     Double_t             MET_RefFinal_phi;
     Double_t             MET_RefFinal_m;
     Int_t                veto_elecs_n;
     std::vector<double>  *veto_elecs_pT;
     std::vector<double>  *veto_elecs_eta;
     std::vector<double>  *veto_elecs_phi;
     Int_t                veto_muons_n;
     std::vector<double>  *veto_muons_pT;
     std::vector<double>  *veto_muons_eta;
     std::vector<double>  *veto_muons_phi;
     Bool_t               EF_e24vhi_medium1;
     Bool_t               EF_e60_medium1;
     Bool_t               EF_mu24i_tight;
     Bool_t               EF_mu36_tight;
     Int_t                ph_n;
     std::vector<double>  *ph_pT;
     std::vector<double>  *ph_cl_eta;
     std::vector<double>  *ph_cl_phi;
     std::vector<double>  *ph_etas2;
     std::vector<int>     *ph_isConverted;
     std::vector<int>     *ph_isTightPhoton;
     Bool_t               EF_g20_loose;
     Bool_t               EF_g40_loose;
     Bool_t               EF_g60_loose;
     Bool_t               EF_g80_loose;
     Bool_t               EF_g100_loose;
     Bool_t               EF_g120_loose;
     Int_t                photonCleanedJets_n;
     std::vector<double>  *photonCleanedJets_pT;
     std::vector<double>  *photonCleanedJets_eta;
     std::vector<double>  *photonCleanedJets_phi;
     std::vector<double>  *photonCleanedJets_m;

     // List of branches
     TBranch        *b_RunNumber;   //!
     TBranch        *b_EventNumber;   //!
     TBranch        *b_mc_channel_number;   //!
     TBranch        *b_dataStream;   //!
     TBranch        *b_pileupWeight;   //!
     TBranch        *b_averageIntPerXing;   //!
     TBranch        *b_avgIntPerXingCorrected;   //!
     TBranch        *b_mc_event_weight;   //!
     TBranch        *b_zvtxweight;   //!
     TBranch        *b_trigger_weight;   //!
     TBranch        *b_el_n;   //!
     TBranch        *b_el_charge;   //!
     TBranch        *b_el_pT;   //!
     TBranch        *b_el_eta;   //!
     TBranch        *b_el_phi;   //!
     TBranch        *b_el_Reco_SF;   //!
     TBranch        *b_el_ID_SF;   //!
     TBranch        *b_el_isTriggerMatched;   //!
     TBranch        *b_el_isEF_e24vhi_medium1Matched;   //!
     TBranch        *b_el_isEF_e60_medium1Matched;   //!
     TBranch        *b_mu_staco_n;   //!
     TBranch        *b_mu_staco_charge;   //!
     TBranch        *b_mu_staco_pT;   //!
     TBranch        *b_mu_staco_eta;   //!
     TBranch        *b_mu_staco_phi;   //!
     TBranch        *b_mu_staco_Reco_SF;   //!
     TBranch        *b_mu_staco_isTriggerMatched;   //!
     TBranch        *b_mu_staco_isEF_mu24i_tightMatched;   //!
     TBranch        *b_mu_staco_isEF_mu36_tightMatched;   //!
     TBranch        *b_jet_n;   //!
     TBranch        *b_jet_pT;   //!
     TBranch        *b_jet_eta;   //!
     TBranch        *b_jet_phi;   //!
     TBranch        *b_jet_m;   //!
     TBranch        *b_MET_RefFinal_pT;   //!
     TBranch        *b_MET_RefFinal_eta;   //!
     TBranch        *b_MET_RefFinal_phi;   //!
     TBranch        *b_MET_RefFinal_m;   //!
     TBranch        *b_veto_elecs_n;   //!
     TBranch        *b_veto_elecs_pT;   //!
     TBranch        *b_veto_elecs_eta;   //!
     TBranch        *b_veto_elecs_phi;   //!
     TBranch        *b_veto_muons_n;   //!
     TBranch        *b_veto_muons_pT;   //!
     TBranch        *b_veto_muons_eta;   //!
     TBranch        *b_veto_muons_phi;   //!
     TBranch        *b_EF_e24vhi_medium1;   //!
     TBranch        *b_EF_e60_medium1;   //!
     TBranch        *b_EF_mu24i_tight;   //!
     TBranch        *b_EF_mu36_tight;   //!
     TBranch        *b_ph_n;   //!
     TBranch        *b_ph_pT;   //!
     TBranch        *b_ph_cl_eta;   //!
     TBranch        *b_ph_cl_phi;   //!
     TBranch        *b_ph_etas2;   //!
     TBranch        *b_ph_isConverted;   //!
     TBranch        *b_ph_isTightPhoton;   //!
     TBranch        *b_EF_g20_loose;   //!
     TBranch        *b_EF_g40_loose;   //!
     TBranch        *b_EF_g60_loose;   //!
     TBranch        *b_EF_g80_loose;   //!
     TBranch        *b_EF_g100_loose;   //!
     TBranch        *b_EF_g120_loose;   //!
     TBranch        *b_photonCleanedJets_n;   //!
     TBranch        *b_photonCleanedJets_pT;   //!
     TBranch        *b_photonCleanedJets_eta;   //!
     TBranch        *b_photonCleanedJets_phi;   //!
     TBranch        *b_photonCleanedJets_m;   //!

};

#endif // ZZSELECTOR_H
