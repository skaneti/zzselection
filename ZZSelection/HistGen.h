/////////////////////////////////////////////////////////////////////////////
//  file: HistGen.h
//  Author: Steven Kaneti <Steven.KanetiSPAMNOTcern.ch>
//
//  Description: This class contains pointers to histograms that will be filled 
//               during the event loop.  The histograms are grouped by photon pT bin, i.e. 
//               25to45, 45to65, 65to85, 85to105, 105to125, 125plus.  THus each instance of this class should 
//               carry a set of histograms that will be filled for a particular prescaled trigger.
//               The filling is done in the generic method Fill().
//
/////////////////////////////////////////////////////////////////////////////

#ifndef HISTGEN_H
#define HISTGEN_H

#include <string>

#include "TFile.h"
#include "TH1D.h"

#include "ZZSelection/DataFields.h"

class HistGen
{
    public:
       HistGen(); // default constructor
       HistGen(const std::string& name);

       ~HistGen();
       void FillHists(const DataFields& df);
       void FillHistsEEChan(const DataFields& df);
       void FillHistsMuMuChan(const DataFields& df);

       void WriteHists();

       std::string getHGName() {return hgname;}

       void setHGName(std::string nm) {hgname = nm;}
      

    private:
       std::string hgname;

       TH1D* hgetmiss_eechan;
       TH1D* hgjetn_eechan;
       TH1D* hgaxialmet_eechan;
       TH1D* hgdphiphotz_eechan;
       TH1D* hgfracptdiff_eechan;
  
       TH1D* hgetmiss_mumuchan;
       TH1D* hgjetn_mumuchan;
       TH1D* hgaxialmet_mumuchan;
       TH1D* hgdphiphotz_mumuchan;
       TH1D* hgfracptdiff_mumuchan;
       
};

#endif // HISTGEN_H
