#!/bin/bash 

scriptname=runzzlocal
echo "sourcing $scriptname on `date -u`" on `hostname`
echo "current working directory is `pwd`"
echo "looking for sub-directories in /r02/atlas/skaneti"


zzBaseDir="/r02/atlas/skaneti/ZZoutputGrid"
outputDir="/r02/atlas/skaneti/ZZoutputGrid/ZZSelection/data"

tag='2013Mar040253'

executable="zzselector"

for j in `ls -d $zzBaseDir/*$tag*`
do
  inputDir=`echo $j | awk 'BEGIN { FS="/" } { print $6 }'`
  #echo "inputDir=$inputDir"
  dataYear=`echo "$inputDir" | awk 'BEGIN {FS="."} {print $4}'`
  #echo "dataYear=$dataYear"
  runNumber=`echo $inputDir | awk 'BEGIN {FS="."} {print $5}'`
  datastream=`echo $inputDir | awk 'BEGIN {FS="."} {print $6}'`
  echo "datastream=$datastream"
  timestamp=`echo $inputDir | awk 'BEGIN {FS="."} {print $7}'`
  echo "timestamp=$timestamp"
  downloadID=`echo $inputDir | awk 'BEGIN {FS="."} {print $8}'`
  outROOTFile="zzselxn.$dataYear.$runNumber.$dataStream$timestamp.$downloadID.root"
  echo "./$executable --inputTChain=$j/*.root --outputFileDirectory=$outputDir --outputFileName=$outROOTFile"
  ./$executable --inputTChain=$j/*.root --outputFileDirectory=$outputDir --outputFileName=$outROOTFile
done
